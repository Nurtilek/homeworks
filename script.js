const root = document.getElementById('root');

const gallery = document.createElement('div');
gallery.classList.add('gallery');
root.appendChild(gallery);

function drawGallery(title, url) {

    const photo = document.createElement('div');
    photo.classList.add('photo');

    const photoTitle = document.createElement('div');
    photoTitle.classList.add('photo__title');
    photoTitle.textContent = title;

    const photoImg = document.createElement('div');
    photoImg.classList.add('photo__img');
    photoImg.style.background = `url(${url})`;
    photoImg.style.backgroundPosition = 'center';
    photoImg.style.backgroundSize = 'cover';

    photo.appendChild(photoImg);
    photo.appendChild(photoTitle);

    return photo;
}

fetch('https://jsonplaceholder.typicode.com/photos')
    .then(
        function(response){
            return response.json();
        }
    )
    .then(
        function(data){
            for(let i = 0; i < data.length; i++) {
                const photo = drawGallery(
                     data[i].title,
                     data[i].url)
            gallery.appendChild(photo);
            }
            
        }  
    )


    


    










